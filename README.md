# MINIGRID-RL
## About
Reinforcement learning algorithms implemented to solve various environments (mostly [Lunar Lander](https://gym.openai.com/envs/LunarLander-v2/)) including [MiniGrid](https://github.com/maximecb/gym-minigrid).  
Runs can be found [here](https://app.wandb.ai/cichyt/rl-experiments).

Main libraries used:
1. PyTorch
1. Numpy
1. gym
1. MiniGrid
1. wandb
1. stable-baselines-3

Algorithms implemented:
1. Deep Q-Learning
1. Double Deep Q-Learning

## Gifs and graphs
### DQN
![LanderDDQN Gif](./media/LanderDqn.gif)
![LanderDDQN Reward](./media/LanderDqn.png)
![LanderDDQN Eps](./media/LanderDqnEps.png)

### Double DQN
![LanderDDQN Gif](./media/LanderDdqn.gif)
![LanderDDQN Reward](./media/LanderDdqn.png)
![LanderDDQN Eps](./media/LanderDdqnEps.png)

## References
1. [Playing Atari with Deep Reinforcement Learning](https://www.cs.toronto.edu/~vmnih/docs/dqn.pdf)
1. [Human-level control through deep reinforcement learning](https://web.stanford.edu/class/psych209/Readings/MnihEtAlHassibis15NatureControlDeepRL.pdf)
1. [Deep Reinforcement Learning with Double Q-learning](https://arxiv.org/abs/1509.06461)
1. https://github.com/maximecb/gym-minigrid
1. https://pythonprogramming.net/training-deep-q-learning-dqn-reinforcement-learning-python-tutorial/
1. https://github.com/DLR-RM/stable-baselines3
