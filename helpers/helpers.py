from skimage.color import rgb2gray
import numpy as np


def to_gray(state):
    return np.expand_dims(rgb2gray(state), axis=0)
