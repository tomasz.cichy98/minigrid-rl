from abc import ABC

import torch.nn as nn
import torch.nn.functional as F


class DqnCnn(nn.Module):
    def __init__(self, c_in, h, w, outputs):
        super(DqnCnn, self).__init__()
        self.conv1 = nn.Conv2d(c_in, 16, kernel_size=2, stride=1)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=2, stride=1)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=2, stride=1)
        self.bn3 = nn.BatchNorm2d(32)

        # Number of Linear input connections depends on output of conv2d layers
        # and therefore the input image size, so compute it.
        def conv2d_size_out(size, kernel_size=2, stride=1):
            return (size - (kernel_size - 1) - 1) // stride + 1
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        self.linear_input_size = convw * convh * 32
        self.lin_1 = nn.Linear(self.linear_input_size, 100)
        self.head = nn.Linear(100, outputs)

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = x/255
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = x.view(-1, self.linear_input_size)
        x = F.relu(self.lin_1(x))
        x = self.head(x)
        return x
        # return self.head(x.view(x.size(0), -1))


class DqnMlp(nn.Module):
    def __init__(self, in_size, outputs):
        super(DqnMlp, self).__init__()
        self.in_size = in_size
        self.l1 = nn.Linear(self.in_size, 100)
        self.l2 = nn.Linear(100, 50)
        self.l3 = nn.Linear(50, 50)

        self.head = nn.Linear(50, outputs)

    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = F.relu(self.l3(x))
        return self.head(x)
