from gym_minigrid.wrappers import *
import gym
# import gym_ple
from agents.DQNAgent import DQNAgent
from agents.A2CAgent import A2CAgent
from stable_baselines3 import DQN, PPO, A2C
from stable_baselines3.common.policies import ActorCriticCnnPolicy
from gym.wrappers import AtariPreprocessing, FrameStack
from stable_baselines3.common.atari_wrappers import AtariWrapper


env_type = "box2d"
# env_type = "atari"
# env_type = "ple"
mode = "compact"
baselines = False
algo = "A2C"

if env_type == "atari":
    # env_name = 'BeamRiderNoFrameskip-v0'
    env_name = 'BreakoutNoFrameskip-v0'
    # env_name = "BowlingNoFrameskip-v0"
    env = gym.make(env_name)
    # env = AtariPreprocessing(env)
    # env = FrameStack(env, num_stack=4)
    env = AtariWrapper(env)

if env_type == "box2d":
    env_name = "LunarLander-v2"
    env = gym.make(env_name)

if env_type == "ple":
    env_name = 'FlappyBird-v0'
    env = gym.make(env_name)

if env_type == "minigrid":
    # env_name = 'MiniGrid-Empty-8x8-v0'
    # env_name = "MiniGrid-DistShift1-v0"
    # env_name = "MiniGrid-DoorKey-8x8-v0"
    env_name = "MiniGrid-FourRooms-v0"
    env = gym.make(env_name)
    if mode == "compact":
        env = FullyObsWrapper(env)
        env = ImgObsWrapper(env)  # Get rid of the 'mission' field
    elif mode == "rgb":
        env = RGBImgObsWrapper(env)
        env = ImgObsWrapper(env)  # Get rid of the 'mission' field
    else:
        print("Other mode")

obs = env.reset()
print(f"Obs shape: {obs.shape}")
print(f"Action space: {env.action_space.n}")
print(f"Observation space: {env.observation_space}")

# print(env.max_steps)
print(f"Running {env_name}, baselines: {baselines}, mode: {mode}, env type: {env_type}")

if baselines:
    # model = DQN('MlpPolicy', env, verbose=2)
    model = A2C("CnnPolicy", env, verbose=2)
    # model = DQN('CnnPolicy', env, verbose=2)
    # model = PPO("MlpPolicy", env, verbose=2)

    model.learn(total_timesteps=50_000)

    for _ in range(5):
        obs = env.reset()
        done = False
        while not done:
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, done, info = env.step(action)
            env.render()
            if done:
                obs = env.reset()
        env.close()
else:
    if algo == "DQN":
        # agent = DQNAgent(env, episodes=1_000, input_mode=mode, wandb_log=True, verbose=False)
        agent = DQNAgent(env, policy="mlp", episodes=5_000, input_mode=mode, wandb_log=True, verbose=False)
        # agent = DQNAgent(env, policy="mlp", episodes=1_000, input_mode=mode, wandb_log=False, verbose=False, env_type=env_type)
    if algo == "A2C":
        # agent = A2CAgent(env, env_name, frames=500, log_step=10, wandb_log=False, verbose=True)
        agent = A2CAgent(env, env_name, frames=50_000, wandb_log=True, verbose=False)

    agent.learn()
    agent.render_episode(5)
