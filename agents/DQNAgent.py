import random
from collections import deque

from gym.wrappers import Monitor

import numpy as np
import torch
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
from tqdm import tqdm
import wandb

from helpers.helpers import to_gray
from models.DQNCNNModel import DqnCnn, DqnMlp

random.seed(42)
np.random.seed(42)


class DQNAgent:
    def __init__(self, env, policy, double_dqn=True, replay_memory_size=50_000, min_replay_memory_size=1_000, minibatch_size=64,
                 discount=0.99,
                 update_target_every=10, episodes=5_000, min_epsilon=0.001, aggregate_every=20,
                 min_reward=0.1, input_mode="compact", max_steps=500, env_type="box2d", wandb_log=True, verbose=False,
                 to_gray_bool=False):
        """

        :param env: gym like environment
        :param policy: policy net type, cnn/mlp
        :param double_dqn: bool whether to use double or regular DQN
        :param replay_memory_size:
        :param min_replay_memory_size:
        :param minibatch_size:
        :param discount: for Q update
        :param update_target_every: update target network every n episodes
        :param episodes: number of episodes to run
        :param min_epsilon: for explaration
        :param aggregate_every: log step
        :param min_reward: when to save model to file
        :param input_mode: compact, rgb... for process state function
        :param max_steps: environment max steps
        :param env_type: box2d, minigrid, atari, for process state
        :param wandb_log: log to wandb?
        :param verbose:
        :param to_gray_bool: convert state to gray from rgb?
        """
        print("Init agent.")
        self.env = env
        self.POLICY = policy
        self.DDQN = double_dqn
        self.REPLAY_MEMORY_SIZE = replay_memory_size
        self.MIN_REPLAY_MEMORY_SIZE = min_replay_memory_size
        self.MINIBATCH_SIZE = minibatch_size
        self.DISCOUNT = discount
        self.UPDATE_TARGET_EVERY = update_target_every
        self.EPISODES = episodes
        self.MIN_EPSILON = min_epsilon
        self.epsilon = 1
        self.EPSILON_DECAY = (1.0 - self.MIN_EPSILON) / (0.5 * self.EPISODES)
        print(f"Eps decay: {self.EPSILON_DECAY}")
        self.AGGREGATE_EVERY = aggregate_every
        self.MIN_REWARD = min_reward
        self.INPUT_MODE = input_mode
        self.ENV_TYPE = env_type
        self.WANDB_LOG = wandb_log
        self.VERBOSE = verbose
        self.MAX_STEPS = max_steps
        self.TO_GRAY = to_gray_bool

        self.MODEL_PATH = f"./saved_models/{self.env.unwrapped.spec.id}_eps-{self.EPISODES}_minibatch-{self.MINIBATCH_SIZE}_policy-{self.POLICY}_double-{self.DDQN}.pth"

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Using device: {self.device}")
        self.base_obs = self.env.reset()
        self.n_actions = self.env.action_space.n
        if self.POLICY == "cnn":
            self.screen_height = self.base_obs.shape[0]
            self.screen_width = self.base_obs.shape[1]
            self.channels_in = self.base_obs.shape[2]
            if self.INPUT_MODE == "wrapped":
                self.screen_height = self.base_obs.shape[1]
                self.screen_width = self.base_obs.shape[2]
                self.channels_in = self.base_obs.shape[0]
            if self.INPUT_MODE == "rgb":
                self.channels_in = 1
            print(f"Models with {self.channels_in} channels, height: {self.screen_height}, width: {self.screen_width}")
        print(f"Policy: {self.POLICY}, env type: {self.ENV_TYPE}, double dqn: {self.DDQN}")
        print(f"Model save path: {self.MODEL_PATH}")

        # models
        if self.POLICY == "cnn":
            self.policy_net = DqnCnn(self.channels_in, self.screen_height, self.screen_width, self.n_actions).to(
                self.device)
            self.target_net = DqnCnn(self.channels_in, self.screen_height, self.screen_width, self.n_actions).to(
                self.device)

        if self.POLICY == "mlp":
            self.policy_net = DqnMlp(self.base_obs.shape[0], self.n_actions).to(self.device)
            self.target_net = DqnMlp(self.base_obs.shape[0], self.n_actions).to(self.device)

        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.replay_memory = deque(maxlen=self.REPLAY_MEMORY_SIZE)
        self.target_update_counter = 0

        self.optimizer = optim.Adam(self.policy_net.parameters())
        self.criterion = torch.nn.MSELoss()

        self.episode_rewards = []

    def update_reply_memory(self, transition):
        """
        Add state to the replay memory
        :param transition: (state, action, reward, state, done) tuple
        """
        self.replay_memory.append(transition)

    def train(self, terminal_state):
        """
        Train policy net on a batch taken from the replay memory
        :param terminal_state: is the state terminal when called
        :return:
        """
        # if not enough data to train
        if len(self.replay_memory) < self.MIN_REPLAY_MEMORY_SIZE:
            return

        # get minibatch
        minibatch = random.sample(self.replay_memory, self.MINIBATCH_SIZE)
        batch_state, batch_action, batch_reward, batch_next_state, batch_done = zip(*minibatch)
        batch_state = Variable(torch.cat(batch_state)).to(self.device)
        batch_action = Variable(torch.cat(batch_action)).to(self.device)
        batch_reward = Variable(torch.cat(batch_reward)).to(self.device)
        batch_next_state = Variable(torch.cat(batch_next_state)).to(self.device)

        # done_mask = Variable(torch.cat(batch_done)).to(self.device).type(torch.LongTensor)
        # non_final_mask = 1 - done_mask
        # non_final = torch.LongTensor([i for i, done in enumerate(done_mask) if not done]).to(self.device)
        # non_final_next_states = Variable(batch_next_state.index_select(0, non_final)).to(self.device)

        # data for the formula (regular dqn
        # Q(s, a) <- Q(s,a) + lr * [ reward + discount * max_next_q - Q(s, a) ]
        # Q(s_t, a)
        current_qs = self.policy_net(batch_state).gather(1, batch_action)

        v_next_state = Variable(torch.zeros(self.MINIBATCH_SIZE).type(torch.Tensor)).to(self.device)
        if self.DDQN:
            # predict an action for the next state
            _, next_actions = self.policy_net(batch_next_state).max(1, keepdim=True)
            # get q for this action from a different net
            v_next_state = self.target_net(batch_next_state).gather(1, next_actions).reshape(-1, )
        else:
            v_next_state = self.target_net(batch_next_state).detach().max(1)[0]

        # v_next_sate = self.target_net(batch_next_state).detach().max(1)[0]
        expected_qs = batch_reward + (self.DISCOUNT * v_next_state)
        expected_qs = expected_qs.view(-1, 1)

        loss = F.smooth_l1_loss(current_qs, expected_qs)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        if terminal_state:
            self.target_update_counter += 1
        # update target net
        if self.target_update_counter > self.UPDATE_TARGET_EVERY:
            if self.VERBOSE:
                print("Updating target net.")
            self.target_net.load_state_dict(self.policy_net.state_dict())
            self.target_update_counter = 0

    def process_state(self, state):
        if self.TO_GRAY:
            state = to_gray(state)
        if self.ENV_TYPE == "minigrid" and self.INPUT_MODE == "compact":
            state = np.transpose(state, axes=(2, 0, 1))
        if self.ENV_TYPE == "atari":
            state = np.transpose(state, axes=(2, 0, 1))
        if self.POLICY == "mlp":
            state = state.flatten()
        return state

    def learn(self):
        if self.WANDB_LOG:
            wandb.init(project="rl-experiments", name=f"{self.env.unwrapped.spec.id}, {self.EPISODES} episodes, {self.POLICY} policy")
            wandb.watch(self.policy_net)
            wandb.watch(self.target_net)
        for episode in tqdm(range(self.EPISODES), ascii=True, unit="episode"):
            episode_reward = 0
            step = 0

            current_state = self.env.reset()
            # current_state = torch.from_numpy(current_state).type(torch.FloatTensor).to(self.device)
            current_state = self.process_state(state=current_state)
            done = False

            while not done:
                if np.random.random() > self.epsilon:
                    with torch.no_grad():
                        action = self.policy_net(
                            Variable(
                                torch.FloatTensor([current_state])
                            ).type(torch.FloatTensor).to(self.device)
                        ).data.max(1)[1].view(1, 1)
                else:
                    action = torch.LongTensor([[np.random.randint(0, self.n_actions)]]).to(self.device)

                new_state, reward, done, _ = self.env.step(action.item())
                # new_state = torch.from_numpy(new_state).type(torch.FloatTensor).to(self.device)
                new_state = self.process_state(new_state)

                episode_reward += reward

                self.update_reply_memory((torch.FloatTensor([current_state]),
                                          action,  # is a tensor
                                          torch.FloatTensor([reward]),
                                          torch.FloatTensor([new_state]),
                                          torch.FloatTensor([done])))
                self.train(done)
                current_state = new_state
                step += 1
            self.episode_rewards.append(episode_reward)

            # log
            if not episode % self.AGGREGATE_EVERY or episode == 0:
                average_reward = sum(self.episode_rewards[-self.AGGREGATE_EVERY:]) / len(
                    self.episode_rewards[-self.AGGREGATE_EVERY:])
                min_reward = min(self.episode_rewards[-self.AGGREGATE_EVERY:])
                max_reward = max(self.episode_rewards[-self.AGGREGATE_EVERY:])
                if self.WANDB_LOG:
                    log_dict = {
                        "avg_reward": average_reward,
                        "min_reward": min_reward,
                        "max_reward": max_reward,
                        "epsilon": self.epsilon,
                    }
                    wandb.log(log_dict)
                if self.VERBOSE:
                    print(f"avg reward: {average_reward}\tmin reward: {min_reward}\tmax reward: {max_reward}")

            if self.epsilon > self.MIN_EPSILON:
                self.epsilon -= self.EPSILON_DECAY
                self.epsilon = max(self.MIN_EPSILON, self.epsilon)

        torch.save(self.target_net.state_dict(), self.MODEL_PATH)
        wandb.save(self.MODEL_PATH)

    def render_episode(self, n):
        """
        Play n episodes while rendering them, to show progress
        :param n: number of episodes to play
        """
        for ep in range(n):
            done = False
            env_monitor = Monitor(self.env, f'./videos/{self.env.unwrapped.spec.id}_ddqn-{self.DDQN}/run-{ep}', force=True, mode='evaluation')
            current_state = self.process_state(env_monitor.reset())
            while not done:
                env_monitor.render()
                with torch.no_grad():
                    action = self.policy_net(
                        Variable(
                            torch.FloatTensor([current_state])
                        ).type(torch.FloatTensor).to(self.device)
                    ).data.max(1)[1].view(1, 1).item()
                    # action = torch.argmax(self.target_net(torch.FloatTensor([current_state]).to(self.device))).item()
                    print(action)
                new_state, reward, done, _ = env_monitor.step(action)
                new_state = self.process_state(new_state)
                current_state = new_state
