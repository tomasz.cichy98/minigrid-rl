from models.ActorCriticModel import ActorCritic
import torch
import torch.optim as optim
import wandb
from tqdm import tqdm
import numpy as np
import gym
from common.multiprocessing_env import SubprocVecEnv
from gym.wrappers import Monitor


class A2CAgent:
    def __init__(self, env, env_name, num_envs=4, n_eval_runs=20, hidden_size=256, lr=3e-4, num_steps=5, frames=50_000, log_step=1_000, wandb_log=True, verbose=False):
        self.env = env
        self.base_obs = self.env.reset()

        self.n_inputs = self.base_obs.shape[0]
        self.n_actions = self.env.action_space.n
        self.HIDDEN_SIZE = hidden_size
        self.lr = lr
        self.NUM_STEPS = num_steps
        self.FRAMES = frames
        self.N_EVAL_RUNS = n_eval_runs

        print(f"A2C alg\t n_ins: {self.n_inputs}, n_outs: {self.n_actions}")

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Using device: {self.device}")

        self.LOG_STEP = log_step
        self.WANDB_LOG = wandb_log
        self.VERBOSE = verbose

        self.actor_critic_model = ActorCritic(num_inputs=self.n_inputs, num_outputs=self.n_actions, hidden_size=self.HIDDEN_SIZE).to(self.device)
        self.optimizer = optim.Adam(self.actor_critic_model.parameters())

        self.test_rewards = []

        def make_env():
            def _thunk():
                env = gym.make(env_name)
                return env

            return _thunk

        self.envs = [make_env() for i in range(num_envs)]
        self.envs = SubprocVecEnv(self.envs)

        # env = gym.make(env_name)

    def compute_returns(self, next_val, rewards, masks, gamma=0.99):
        R = next_val
        returns = []
        for step in reversed(range(len(rewards))):
            R = rewards[step] + gamma*R*masks[step]
            returns.insert(0, R)
        return returns

    def test_env(self, env, render=False):
        state = env.reset()
        if render: env.render()
        done = False
        total_reward = 0
        while not done:
            state = torch.FloatTensor(state).unsqueeze(0).to(self.device)
            dist, _ = self.actor_critic_model(state)
            next_state, reward, done, _ = env.step(dist.sample().cpu().numpy()[0])
            state = next_state
            if render: env.render()
            total_reward += reward
        return total_reward

    def learn(self):
        if self.WANDB_LOG:
            wandb.init(project="rl-experiments", name=f"{self.env.unwrapped.spec.id}, {self.FRAMES} frames, A2C")
            wandb.watch(self.actor_critic_model)
        for frame in tqdm(range(self.FRAMES), ascii=True, unit="frame"):
            log_probs = []
            values = []
            rewards = []
            masks = []
            entropy = 0
            state = self.envs.reset()

            for _ in range(self.NUM_STEPS):
                state = torch.FloatTensor(state).to(self.device)
                # print(f"state shape: {state.shape}")
                dist, val = self.actor_critic_model(state)

                action = dist.sample()
                # print(f"action: {action}")
                next_state, reward, done, _ = self.envs.step(action.cpu().numpy())

                log_prob = dist.log_prob(action)
                entropy += dist.entropy().mean()

                log_probs.append(log_prob)
                values.append(val)
                rewards.append(torch.FloatTensor(reward).unsqueeze(1).to(self.device))
                masks.append(torch.FloatTensor(1-done).unsqueeze(1).to(self.device))

                state = next_state

            if frame % self.LOG_STEP == 0:
                rews = [self.test_env(self.env) for _ in range(self.N_EVAL_RUNS)]
                # print(rews)
                average_reward = np.mean(rews)
                min_reward = np.min(rews)
                max_reward = np.max(rews)
                self.test_rewards.append(average_reward)
                log_dict = {
                    "avg_reward": average_reward,
                    "min_reward": min_reward,
                    "max_reward": max_reward
                }
                if self.WANDB_LOG:
                    wandb.log(log_dict)
                if self.VERBOSE:
                    print(log_dict)

            next_state = torch.FloatTensor(next_state).to(self.device)
            _, next_value = self.actor_critic_model(next_state)
            returns = self.compute_returns(next_value, rewards, masks)

            log_probs = torch.cat(log_probs)
            returns = torch.cat(returns).detach()
            values = torch.cat(values)

            advantage = returns - values

            actor_loss = -(log_probs * advantage.detach()).mean()
            critic_loss = advantage.pow(2).mean()

            loss = actor_loss + 0.5 * critic_loss - 0.001*entropy

            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

    def render_episode(self, n):
        for ep in range(n):
            env_monitor = Monitor(self.env, f'./videos/{self.env.unwrapped.spec.id}_A2C/run-{ep}', force=True, mode='evaluation')
            self.test_env(env_monitor, render=True)
