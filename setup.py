from setuptools import find_packages, setup

setup(
    name='minigrid-rl',
    packages=find_packages(),
    version='0.1.0',
    description='Reinforcement Learning for minigrid envs.',
    author='Tomasz Cichy',
    license='MIT',
)
